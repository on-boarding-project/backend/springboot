package com.systeric.onboard.modules.users;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import com.systeric.onboard.modules.users.data.UserEntity;
import com.systeric.onboard.modules.users.dto.user.CreateUserDTO;
import com.systeric.onboard.modules.users.repository.UserRepository;
import com.systeric.onboard.modules.users.service.UserService;

@ExtendWith(MockitoExtension.class)
public class UsersServiceTest {
    @InjectMocks
    UserService userService;

    @Mock
    UserRepository userRepository;

    UserEntity user = new UserEntity();
    UserEntity userEntity = new UserEntity();

    CreateUserDTO createUserDTO = new CreateUserDTO();

    @BeforeEach
    public void initUseCase() throws Exception {
        user = new UserEntity("bokir", "hermawan");

        createUserDTO.setFirstName("Bokir");
        createUserDTO.setLastName("Hermawan");
        createUserDTO.setAddress("Jl. Dipatiukur");
        createUserDTO.setDateOfBirth("1980-08-12");

        userEntity = new UserEntity(createUserDTO);

        userEntity = new UserEntity(createUserDTO);
    }

    @Test
    void itShouldSaveUser() throws Exception {

        when(userRepository.save(any(UserEntity.class))).thenReturn(userEntity);

        UserEntity expected = userEntity;

        UserEntity result = userService.save(userEntity);

        assertThat(result.getFirstName().getFirstName()).isEqualTo(expected.getFirstName().getFirstName());
    }

    @Test
    void itShouldUpdateUser() throws Exception {

        when(userRepository.save(any(UserEntity.class))).thenReturn(userEntity);

        UserEntity expected = userEntity;

        UserEntity result = userService.update(userEntity);

        assertThat(result.getFirstName().getFirstName()).isEqualTo(expected.getFirstName().getFirstName());
    }

    @Test
    void itShouldGet() {
        when(userRepository.getById(any(Integer.class))).thenReturn(user);
        assertNotNull(userService.get(1));

    }

    @Test
    void itShouldFindAll() {
        when(userRepository.findByContaining(any(String.class), any(Pageable.class))).thenReturn(new PageImpl<>(List.of(user)));
        assertEquals(userService.findAll("", 1, 1, "name").getContent().get(0), user);

    }

    @Test
    void itShouldDelete() throws Exception {
        Mockito.doNothing().when(userRepository).delete(any(UserEntity.class));
        user.setId(1);
        userService.delete(user);
        verify(userRepository, times(1)).delete(any(UserEntity.class));
    }
}
