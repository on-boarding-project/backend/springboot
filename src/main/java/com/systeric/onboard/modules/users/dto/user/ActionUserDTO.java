package com.systeric.onboard.modules.users.dto.user;

import com.systeric.onboard.modules.users.data.UserEntity;
import com.systeric.onboard.utils.NullSafeUtil;
import com.systeric.onboard.vo.FirstName;
import com.systeric.onboard.vo.LastName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ActionUserDTO {
    private Integer id;
    private String firstName;
    private String lastName;

    public ActionUserDTO(UserEntity user) {
        if(user == null) return;

        setId(NullSafeUtil.eval(null, user, UserEntity::getId));
        setFirstName(NullSafeUtil.eval(null, user, UserEntity::getFirstName, FirstName::getFirstName));
        setLastName(NullSafeUtil.eval(null, user, UserEntity::getLastName, LastName::getLastName));
    }
}
