package com.systeric.onboard.modules.users.repository;

import com.systeric.onboard.modules.users.data.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Integer>, JpaSpecificationExecutor<UserEntity>, PagingAndSortingRepository<UserEntity, Integer> {

    long countByDeletedAtIsNull();

    @Override
    Optional<UserEntity> findById(Integer id);

    @Query(nativeQuery = true,
            value = "SELECT u.* FROM users u " +
                    "WHERE (u.deleted_at is NULL " +
                    "AND u.deleted_by is NULL) " +
                    "AND ((:searchTerm = 'NULL' OR LOWER(u.first_name) LIKE CONCAT('%',LOWER(:searchTerm),'%')) " +
                    "OR (:searchTerm = 'NULL' OR LOWER(u.last_name) LIKE CONCAT('%',LOWER(:searchTerm),'%')) " +
                    "OR (:searchTerm = 'NULL' OR LOWER(u.address) LIKE CONCAT('%',LOWER(:searchTerm),'%')) " +
                    "OR (:searchTerm = 'NULL' OR CAST(u.date_of_birth as varchar) LIKE CONCAT('%',LOWER(:searchTerm),'%'))) ")
    Page<UserEntity> findByContaining(@Param("searchTerm") String searchTerm, Pageable paging);

}
