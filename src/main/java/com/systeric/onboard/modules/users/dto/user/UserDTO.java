package com.systeric.onboard.modules.users.dto.user;

import java.io.Serializable;

import com.systeric.onboard.modules.users.data.UserEntity;
import com.systeric.onboard.utils.Converter;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO implements Serializable {
    private static final long serialVersionUID = 8267343629920364863L;
    private Integer id;
    private String firstName;
    private String lastName;
    private String address;
    private String comment;
    private Boolean termsAccepted;
    private String createdAt;
    private String updatedAt;
    private String deletedAt;
    private ActionUserDTO createdBy;

    public UserDTO(UserEntity user) {
        if (user == null)
            return;

        setId(user.getId());
        setFirstName(user.getFirstName().getFirstName());
        setLastName(user.getLastName().getLastName());
        setAddress(user.getAddress().getAddress());
        setCreatedBy(new ActionUserDTO(user.getCreatedBy()));
        setCreatedAt(Converter.timestampToString(user.getCreatedAt()));
        setUpdatedAt(Converter.timestampToString(user.getUpdatedAt()));
        setDeletedAt(Converter.timestampToString(user.getDeletedAt()));
    }
}
