package com.systeric.onboard.modules.users.controller;

import com.systeric.onboard.modules.users.data.UserEntity;
import com.systeric.onboard.modules.users.dto.user.*;
import com.systeric.onboard.modules.users.service.UserService;
import com.systeric.onboard.redis.CustomPageImpl;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.function.Function;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping
    public CustomPageImpl<UserDTO> find(@RequestParam(defaultValue = "0") Integer page,
                                        @RequestParam(defaultValue = "10") Integer limit,
                                        @RequestParam(defaultValue = "id") String sortBy,
                                        @RequestParam(value = "searchTerm", required = false) String searchTerm){
        Page<UserEntity> pages = userService.findAll(searchTerm, page, limit, sortBy);
        return new CustomPageImpl<>(pages.map(new Function<UserEntity, UserDTO>() {
            @Override
            public UserDTO apply(UserEntity user) {
                return new UserDTO(user);
            }
        }));
    }

    @GetMapping("{userId}")
    public UserDTO get(@PathVariable("userId") UserEntity user) {
        return new UserDTO(user);
    }

    @PostMapping
    @CacheEvict(value = { "users" }, allEntries = true)
    public UserDTO save(
            HttpServletRequest request,
            @RequestBody CreateUserDTO user
    ) throws Exception {
        UserEntity newUser = userService.save(new UserEntity(user));
        return new UserDTO(newUser);
    }

    @PutMapping("{userId}")
    @CacheEvict(value = "users", allEntries = true)
    public UserDTO update(
            HttpServletRequest request,
            @RequestBody UpdateUserDTO updateDTO,
            @PathVariable("userId") UserEntity toUpdate
    ) throws Exception {

        toUpdate.applyUpdate(updateDTO);
        UserEntity updatedUser = userService.update(toUpdate);

        return new UserDTO(updatedUser);
    }

    @DeleteMapping("{userId}")
    @CacheEvict(value = "users", allEntries = true)
    public UserDTO delete(@PathVariable("userId") UserEntity user) throws Exception {
        userService.delete(user);
        return new UserDTO(user);
    }
}
