package com.systeric.onboard.modules.users.dto.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateUserDTO {
    private String firstName;

    private String lastName;

    private String address;

    private String dateOfBirth;

    private String comment;
}
