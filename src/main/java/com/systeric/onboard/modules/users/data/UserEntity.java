package com.systeric.onboard.modules.users.data;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.systeric.onboard.base.entity.AuditTrailEntity;
import com.systeric.onboard.interfaces.PersonDetailInterface;
import com.systeric.onboard.modules.products.data.ProductEntity;
import com.systeric.onboard.modules.users.dto.user.CreateUserDTO;
import com.systeric.onboard.modules.users.dto.user.UpdateUserDTO;
import com.systeric.onboard.vo.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.lang.model.element.ModuleElement;
import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@Table(name = "users")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id"
)
@Entity
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "user_gen", sequenceName = "users_id_seq", allocationSize = 1)
@SQLDelete(sql = "UPDATE users SET deleted_at = now() WHERE id=?")
@Proxy(lazy = false)
public class UserEntity extends AuditTrailEntity implements PersonDetailInterface {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "user_gen"
    )
    @Column(name = "id")
    private Integer id;

    @Embedded   //kalo mau pake VO jadi pake ini, kalo ngga pake @Column
    private FirstName firstName;

    @Embedded
    private LastName lastName;

    @Embedded
    private Address address;

    @Embedded
    private DateOfBirth dateOfBirth;

    @Embedded
    private Comment comment;

    //ga wajib, fungsinya cuman waktu user mau liat data user + daftar product nya
    @OneToMany(mappedBy = "user")
    private List<ProductEntity> products;

    public UserEntity(
            String firstName,
            String lastName,
            String address,
            String dateOfBirth,
            String comment
    ) throws Exception {
        this.firstName = new FirstName(firstName);
        this.lastName = new LastName(lastName);
        this.address = new Address(address);
        this.dateOfBirth = new DateOfBirth(dateOfBirth);
        this.comment = new Comment(comment);
    }

    public UserEntity(String firstName, String lastName) throws Exception {
        this.firstName = new FirstName(firstName);
        this.lastName = new LastName(lastName);
    }

    public UserEntity(
            CreateUserDTO create
    ) throws Exception {
        firstName = new FirstName(create.getFirstName());
        lastName = new LastName(create.getLastName());
        address = new Address(create.getAddress());
        dateOfBirth = new DateOfBirth(create.getDateOfBirth());
        comment = new Comment(create.getComment());
    }

    public void applyUpdate(
            UpdateUserDTO dto
    ) throws Exception {
        firstName = dto.getFirstName() == null ? firstName : new FirstName(dto.getFirstName());
        lastName = dto.getLastName() == null ? lastName : new LastName(dto.getLastName());
        address = dto.getAddress() == null ? address : new Address(dto.getAddress());
        dateOfBirth = dto.getDateOfBirth() == null ? dateOfBirth : new DateOfBirth(dto.getDateOfBirth());
        comment = dto.getComment() == null ? comment : new Comment(dto.getComment());
    }

    public void applyDelete() throws Exception {
        deletedAt = new Timestamp(System.currentTimeMillis());
    }
}
