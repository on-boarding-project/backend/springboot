package com.systeric.onboard.modules.users.service;

import com.systeric.onboard.modules.users.data.*;
import com.systeric.onboard.modules.users.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public UserEntity get(Integer userId) {
        return userRepository.getById(userId);
    }

    public Page<UserEntity> findAll(String searchTerm, Integer page, Integer limit, String sortBy) {
        Pageable paging = PageRequest.of(page, limit, Sort.by(sortBy));
        return userRepository.findByContaining(searchTerm == null || searchTerm.equals("") ? "NULL" : searchTerm, paging);
    }

    @Transactional
    public UserEntity save(UserEntity user) throws Exception {
        UserEntity newUser = userRepository.save(user);
        return newUser;
    }

    public UserEntity update(UserEntity toUpdate) throws Exception {
        return userRepository.save(toUpdate);
    }

    @Transactional
    public void delete(UserEntity user) throws Exception {
        userRepository.delete(user);
    }

    public Long count(Specification<UserEntity> specs) {
        return userRepository.count(specs);
    }
}
