package com.systeric.onboard.modules.products.dto.product;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CreateProductDTO {
    private String name;
    private String description;
    private Integer userId;
}
