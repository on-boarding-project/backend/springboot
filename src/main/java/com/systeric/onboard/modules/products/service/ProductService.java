package com.systeric.onboard.modules.products.service;

import com.systeric.onboard.modules.products.data.ProductEntity;
import com.systeric.onboard.modules.products.dto.product.CreateProductDTO;
import com.systeric.onboard.modules.products.repository.ProductRepository;
import com.systeric.onboard.modules.users.data.UserEntity;
import com.systeric.onboard.modules.users.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Service
@AllArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    public ProductEntity save(CreateProductDTO request) {
        UserEntity user = userRepository.getById(request.getUserId());
        ProductEntity product = new ProductEntity(request, user);
        return productRepository.save(product);
    }

    public ProductEntity findOne(Integer id) {
        Optional<ProductEntity> productEntity = productRepository.findById(id);
        if (!productEntity.isPresent()) {
            return null;
        }
        return productEntity.get();
    }

    public Iterable<ProductEntity> findAll() {
        return productRepository.findAll();
    }

    public void removeOne(Integer id) {
        productRepository.deleteById(id);
    }

    public ProductEntity update(CreateProductDTO requestUpdate, Integer id) {
        ProductEntity productEntity = productRepository.getById(id);
        if (productEntity == null) {
            return null;
        }
        else {
            UserEntity user = userRepository.getById(requestUpdate.getUserId());
            productEntity.updateProduct(requestUpdate,user);
        }
        return productRepository.save(productEntity);
    }
}
