package com.systeric.onboard.modules.products.controller;

import com.systeric.onboard.modules.products.data.ProductEntity;
import com.systeric.onboard.modules.products.dto.product.CreateProductDTO;
import com.systeric.onboard.modules.products.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    @PostMapping
    public ResponseEntity<ProductEntity> create(@RequestBody CreateProductDTO requestProduct) {
        ProductEntity newProduct = productService.save(requestProduct);
        return ResponseEntity.ok(newProduct);
    }

    @GetMapping
    public Iterable<ProductEntity> findAll() {
        return productService.findAll();
    }

    @GetMapping("/{id}")
    public ProductEntity findOne(@PathVariable("id") Integer id) {
        return productService.findOne(id);
    }

    @PutMapping("{id}")
    public ResponseEntity<ProductEntity> update(
            @RequestBody CreateProductDTO requestUpdateProduct,
            @PathVariable("id") Integer id) {
        ProductEntity updateProduct = productService.update(requestUpdateProduct, id);
        //kalo mau ngereturn harus pake function ok ini
        return ResponseEntity.ok(updateProduct);
    }

    @DeleteMapping("/{id}")
    public void removeOne(@PathVariable("id") Integer id){
        productService.removeOne(id);
    }
}
