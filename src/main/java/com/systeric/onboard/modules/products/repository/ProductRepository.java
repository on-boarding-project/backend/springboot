package com.systeric.onboard.modules.products.repository;

import com.systeric.onboard.modules.products.data.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductRepository extends JpaRepository<ProductEntity, Integer>, JpaSpecificationExecutor<ProductEntity>, PagingAndSortingRepository<ProductEntity, Integer> {
}
