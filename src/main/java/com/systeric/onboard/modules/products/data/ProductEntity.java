package com.systeric.onboard.modules.products.data;

import com.systeric.onboard.base.entity.AuditTrailEntity;
import com.systeric.onboard.modules.products.dto.product.CreateProductDTO;
import com.systeric.onboard.modules.users.data.UserEntity;
import lombok.*;
import org.hibernate.annotations.SQLDelete;

import javax.persistence.*;



@Entity
@Table(name = "products")
@Setter
@Getter
@NoArgsConstructor
@SequenceGenerator(name = "product_gen", sequenceName = "product_id_seq", allocationSize = 1)
@SQLDelete(sql = "UPDATE tbl_product SET deleted_at = now() WHERE id=?")
public class ProductEntity extends AuditTrailEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "product_gen")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    public ProductEntity (CreateProductDTO createProductDTO, UserEntity user) {
        this.name = createProductDTO.getName();
        this.description = createProductDTO.getDescription();
        this.user = user;
    }

    public void updateProduct(CreateProductDTO createProductDTO, UserEntity user) {
        this.name = createProductDTO.getName();
        this.description = createProductDTO.getDescription();
        this.user = user;
    }
}
