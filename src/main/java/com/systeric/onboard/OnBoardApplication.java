package com.systeric.onboard;

import com.systeric.onboard.modules.products.data.ProductEntity;
import com.systeric.onboard.modules.products.repository.ProductRepository;
import com.systeric.onboard.modules.users.data.UserEntity;
import com.systeric.onboard.modules.users.repository.UserRepository;
import com.systeric.onboard.vo.*;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication ( exclude = {SecurityAutoConfiguration.class} )
@EnableCaching
@EnableScheduling
public class OnBoardApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnBoardApplication.class, args);
    }

}
