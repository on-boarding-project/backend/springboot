package com.systeric.onboard.redis;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CustomPageImpl<T> {

    private int number;
    private int size;
    private int totalPages;
    private int numberOfElements;
    private long totalElements;
    private boolean previousPage;
    private boolean first;
    private boolean nextPage;
    private boolean last;
    private List<T> content;

    public CustomPageImpl(Page<T> page) {
        setNumber(page.getNumber());
        setSize(page.getSize());
        setTotalPages(page.getTotalPages());
        setNumberOfElements(page.getNumberOfElements());
        setTotalElements(page.getTotalElements());
        setPreviousPage(page.hasPrevious());
        setFirst(page.isFirst());
        setNextPage(page.hasNext());
        setLast(page.isLast());
        setContent(page.getContent());
    }

}