package com.systeric.onboard.redis;

import org.springframework.cache.interceptor.KeyGenerator;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CustomCacheKeyGenerator implements KeyGenerator {

    @Override
    public Object generate(Object target, Method method, Object... params) {
        List<String> stringParams = Arrays.stream(params).map(param -> {
            String str = Objects.toString(param, "");
            if (str.startsWith("com.sipios.springsearch.SpecificationImpl"))
                return "com.sipios.springsearch.SpecificationImpl"; // Removes checksum to prevent over commiting cache

            return str;
        }).collect(Collectors.toList());

        return target.getClass().getSimpleName() + "_"
                + method.getName() + "_"
                + String.join("_", stringParams);
    }
}

