package com.systeric.onboard.base.entity;

import com.systeric.onboard.modules.users.data.UserEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.*;
import java.sql.Timestamp;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
public class AuditTrailEntity {
    @CreationTimestamp
    @Column(name = "created_at", updatable = false)
    protected Timestamp createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    protected Timestamp updatedAt;

    @Column(name = "deleted_at")
    protected Timestamp deletedAt;

    @OneToOne
    @JoinColumn(
            name = "created_by",
            referencedColumnName = "id"
    )
    @CreatedBy
    protected UserEntity createdBy;

    @OneToOne
    @JoinColumn(
            name = "updated_by",
            referencedColumnName = "id"
    )
    @LastModifiedBy
    protected UserEntity updatedBy;

    @OneToOne
    @JoinColumn(
            name = "deleted_by",
            referencedColumnName = "id"
    )
    protected UserEntity deletedBy;
}
