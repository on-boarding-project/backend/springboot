package com.systeric.onboard.base.seed;

public interface ISeed {
    void seed() throws Exception;
}
