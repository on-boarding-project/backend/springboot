package com.systeric.onboard.dto;

import lombok.Getter;

@Getter
public class PaginationResult<T> {
    private T data;

    private Long count;

    public PaginationResult(T data, Long count) {
        this.data = data;
        this.count = count;
    }

}
