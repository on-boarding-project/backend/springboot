package com.systeric.onboard.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class BulkUploadDTO {
    private Boolean success;
    private Integer inserted;
    private String message;
}
