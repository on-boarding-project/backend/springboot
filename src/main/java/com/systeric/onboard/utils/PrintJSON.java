package com.systeric.onboard.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PrintJSON {

    public static <T> String print(T obj) throws JsonProcessingException {
        String jsonString = new ObjectMapper()
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(obj);
        return jsonString;
    }
}
