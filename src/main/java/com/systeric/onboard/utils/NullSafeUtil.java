package com.systeric.onboard.utils;

import java.util.Optional;
import java.util.function.Function;

public class NullSafeUtil {
    public static <R, T, A> R eval(
            R defaultValue,
            T input,
            Function<T, R> function
    ) {
        return Optional.ofNullable(input)
                .map(function)
                .orElse(defaultValue);
    }

    public static <R, T, A> R eval(
            R defaultValue,
            T input,
            Function<T, A> function,
            Function<A, R> function2) {
        return Optional.ofNullable(input)
                .map(function)
                .map(function2)
                .orElse(defaultValue);
    }
}