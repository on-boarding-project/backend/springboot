package com.systeric.onboard.utils;

public class Util {
    public static Boolean isEmpty(String value) {
        return value == null || value.trim().isEmpty();
    }

    public static Boolean isEmpty(Integer value) {
        return value == null || value.toString().trim().isEmpty();
    }

    public static Boolean isEmpty(Boolean value) {
        return value == null || value.toString().trim().isEmpty();
    }

    public static String capitalize(String word) {
        String firstLetter = word.substring(0, 1);
        String remainingLetter = word.substring(1);

        return firstLetter.toUpperCase() + remainingLetter;
    }
}
