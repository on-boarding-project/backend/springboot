package com.systeric.onboard.utils;

import org.joda.time.DateTime;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

public class Converter {
    public static String dateToString(Date date) {
        if (date == null) return null;
        return new DateTime(date).toDateTimeISO().toString().substring(0,10);
    }

    public static String timestampToString(Timestamp timestamp) {
        if (timestamp == null) return null;
        return new DateTime(timestamp).toDateTimeISO().toString();
    }

    public static String timeToString(Time time) {
        if(time == null) return null;
        return time.toString();
    }
}
