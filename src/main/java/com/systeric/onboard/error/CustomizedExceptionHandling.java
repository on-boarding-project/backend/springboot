package com.systeric.onboard.error;

import com.systeric.onboard.exception.ValidationException;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class CustomizedExceptionHandling {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<CustomErrorResponse> handleGenericException(Exception e) {
        this.printErr(e);
        CustomErrorResponse error = new CustomErrorResponse(e.getLocalizedMessage(), e.getMessage());
        error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.INTERNAL_SERVER_ERROR.value()));
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<CustomErrorResponse> handleGenericNotFoundException(Exception e) {
        CustomErrorResponse error = new CustomErrorResponse(e.getLocalizedMessage(), e.getMessage());
        error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.NOT_FOUND.value()));
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ValidationException.class)
    public ResponseEntity<CustomErrorResponse> handleGenericBadRequestException(Exception e) {
        CustomErrorResponse error = new CustomErrorResponse(e.getLocalizedMessage(), e.getMessage());
        error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.BAD_REQUEST.value()));
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    private void printErr(Exception e) {
        StackTraceElement[] traces = e.getStackTrace();
        if(traces.length > 0) System.err.println(traces[0]);
    }
}