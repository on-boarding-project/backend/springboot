package com.systeric.onboard.interfaces;

import com.systeric.onboard.vo.FirstName;
import com.systeric.onboard.vo.LastName;

public interface PersonDetailInterface {
    FirstName getFirstName();

    LastName getLastName();

    default String getFullName() {
        if (getFirstName() == null && getLastName() == null) return null;
        else if (getFirstName() == null && getLastName() != null) return getLastName().getLastName();
        else if (getLastName() == null && getFirstName() != null) return getFirstName().getFirstName();

        return getFirstName().getFirstName() + " " + getLastName().getLastName();
    }

    default String getInitial() {
        if (getFirstName() == null && getLastName() == null) return null;
        else if (getFirstName() == null && getLastName() != null) return getLastName().getLastName().charAt(0) + "";
        else if (getLastName() == null && getFirstName() != null) return getFirstName().getFirstName().charAt(0) + "";

        return getFirstName().getFirstName().charAt(0) + "" + getLastName().getLastName().charAt(0);
    }

}
