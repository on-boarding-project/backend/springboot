package com.systeric.onboard.vo;

import com.systeric.onboard.exception.ValidationException;
import com.systeric.onboard.exception.ValidationExceptionMessages;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class FirstName {
    @Column(
            name = "first_name",
            length = 30,
            nullable = false
    )
    private String firstName;

    public FirstName(String firstName) throws ValidationException {
        if (firstName == null)
            throw new ValidationException(String.format(ValidationExceptionMessages.VALIDATION_NULL.getMessage(), "First Name"));
        if (firstName.trim().length() < 2)
            throw new ValidationException(String.format(ValidationExceptionMessages.VALIDATION_LESS_LENGTH.getMessage(), "First Name", 2));
        if (firstName.trim().length() > 16)
            throw new ValidationException(String.format(ValidationExceptionMessages.VALIDATION_OVER_LENGTH.getMessage(), "First Name", 16));
        this.firstName = firstName.trim();
    }
}
