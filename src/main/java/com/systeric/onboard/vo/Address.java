package com.systeric.onboard.vo;

import com.systeric.onboard.exception.ValidationException;
import com.systeric.onboard.exception.ValidationExceptionMessages;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class Address {
    @Column(
            name = "address",
            nullable = false
    )
    private String address;

    public Address(String address) throws ValidationException {
        if (address == null)
            throw new ValidationException(String.format(ValidationExceptionMessages.VALIDATION_NULL.getMessage(), "address"));
        if (address.trim().length() < 14)
            throw new ValidationException(String.format(ValidationExceptionMessages.VALIDATION_LESS_LENGTH.getMessage(), "address", 14));
        if (address.trim().length() > 130)
            throw new ValidationException(String.format(ValidationExceptionMessages.VALIDATION_OVER_LENGTH.getMessage(), "address", 130));

        this.address = address.trim();
    }
}
