package com.systeric.onboard.vo;

import com.systeric.onboard.exception.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class Comment {
    @Column(
            name = "comment",
            columnDefinition = "TEXT"
    )
    private String comment;

    public Comment(String comment) throws ValidationException {
        this.comment = comment;
    }
}
