package com.systeric.onboard.vo;

import com.systeric.onboard.exception.ValidationException;
import com.systeric.onboard.exception.ValidationExceptionMessages;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class LastName {
    @Column(
            name = "last_name",
            length = 30
    )
    private String lastName;

    public LastName(String lastName) throws ValidationException {
        if(lastName!=null){
            if (lastName.trim().length() < 2)
                throw new ValidationException(String.format(ValidationExceptionMessages.VALIDATION_LESS_LENGTH.getMessage(), "last name", 3));
            if (lastName.trim().length() > 16)
                throw new ValidationException(String.format(ValidationExceptionMessages.VALIDATION_OVER_LENGTH.getMessage(), "last name", 16));
            lastName = lastName.trim();
        }
        this.lastName = lastName;
    }
}
