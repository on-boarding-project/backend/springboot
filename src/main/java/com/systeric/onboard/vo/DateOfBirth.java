package com.systeric.onboard.vo;

import com.systeric.onboard.exception.ValidationException;
import com.systeric.onboard.exception.ValidationExceptionMessages;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
public class DateOfBirth {
    @Column(
            name = "date_of_birth"
    )
    private java.sql.Date dateOfBirth;

    public DateOfBirth(String dateOfBirth) throws ValidationException {
       if(dateOfBirth == null || dateOfBirth.trim().isEmpty()){
           this.dateOfBirth = null;
       }else{
           try{
               this.dateOfBirth = java.sql.Date.valueOf(dateOfBirth);
           }catch (Exception e){
               throw new ValidationException(String.format(ValidationExceptionMessages.VALIDATION_VALUE_INVALID.getMessage(),"date of birth"));
           }
       }

    }
}
