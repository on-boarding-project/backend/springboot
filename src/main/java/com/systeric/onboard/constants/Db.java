package com.systeric.onboard.constants;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class Db {
    public final int BATCH_SIZE = 800;
}
