create sequence if not exists product_id_seq start 1 increment 1;
create table products
(
    id               int4         not null,
    created_at       timestamp,
    deleted_at       timestamp,
    updated_at       timestamp,
    name       varchar(30)  not null,
    description        varchar(30)  not null,
    created_by       int4,
    deleted_by       int4,
    updated_by       int4,
    primary key (id)
);