create sequence users_id_seq start 1 increment 1;
create table users
(
    id               int4         not null,
    created_at       timestamp,
    deleted_at       timestamp,
    updated_at       timestamp,
    address          varchar(255) not null,
    comment          TEXT,
    date_of_birth    date,
    first_name       varchar(30)  not null,
    last_name        varchar(30)  not null,
    created_by       int4,
    deleted_by       int4,
    updated_by       int4,
    primary key (id)
);
alter table users
    add constraint FKibk1e3kaxy5sfyeekp8hbhnim foreign key (created_by) references users;
alter table users
    add constraint FKtd2l28q3oe9v164nps61hxt1f foreign key (deleted_by) references users;
alter table users
    add constraint FKci7xr690rvyv3bnfappbyh8x0 foreign key (updated_by) references users;